<?php

namespace WSIServices\TonicComponents;

/**
 * \WSIServices\TonicComponents\Resource extends \Tonic\Resource
 * - Provides alternate (@routeMethod) annotation to @method
 * - Direct call to parse request data
 * - Simplified access to request uri
 * - Container tie-in
 * - Callable handling
 *
 * @author Sam Likins
 */
class Resource extends \Tonic\Resource {

	protected $container;

	protected $callable = array();

	/**
     * HTTP method condition must match request method
	 * Redirect of Tonic Resource method to rename anotation from predefined name
     * @param string $method
	 * @return null 
	 */
	protected function routeMethod($method) {
		return parent::method($method);
	}

	/**
	 * Parse Tonic Request data
	 * @param boolean $pullGetInfo [optional] Retrieve $_GET data
	 * @return array 
	 */
	protected function parseRequestData($pullGetInfo = false) {
		$data = array();
		if(is_string($this->request->data))
			parse_str($this->request->data, $data);

		if($pullGetInfo)
			$data += $_GET;

		return $this->params += $data;
	}

	/**
	 * Set resource container
	 * @param mixed $container
	 * @return Resource 
	 */
	public function setContainer($container) {
		$this->container = $container;
		return $this;
	}

	/**
	 * Retrieve Tonic Request uri
	 * @return string 
	 */
	public function getUri() {
		return $this->request->uri;
	}

	/**
	 * Set callable for use with magic methods
	 *
	 * <code>
	 * $res->setCallable('renderTemplate', function($template) {
	 * 	return new Tonic\Response(
	 * 		Tonic\Response::OK,
	 * 		$this->container->Twig()->render($template)
	 * 	);
	 * })
	 * 	->setCallable('getEntityManager', function() {
	 * 		return $this->container->entityManager();
	 * 	});
	 * </code>
	 *
	 * @param string $methodName
	 * @param Closure $closure
	 * @return Resource 
	 */
	public function setCallable($methodName, \Closure $closure) {
		$this->callable[$methodName] = $closure->bindTo($this, get_class());

		return $this;
	}

	/**
	 * Removes previously set callable
	 * @param string $methodName
	 * @return Resource 
	 */
	public function clearCallable($methodName) {
		unset($this->callable[$methodName]);

		return $this;
	}

	/**
	 * Calls previously set callable, throws exception otherwise
	 * @param string $name
	 * @param array $arguments
	 * @return mixed
	 * @throws \BadMethodCallException 
	 */
	public function __call($name, $arguments) {
		if(array_key_exists($name, $this->callable))
			return call_user_func_array($this->callable[$name], $arguments);
			//return $this->callable[$name]($arguments);

		throw new \BadMethodCallException('Requested method `'.$name.'` was not defined with Resource.');
	}

}