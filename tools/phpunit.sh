#!/bin/bash

BASEDIR=$(dirname $0)
TESTDIR=`readlink -f "$BASEDIR/../test"`

phpunit --stop-on-error --bootstrap "$TESTDIR/bootstrap.php" "$TESTDIR"