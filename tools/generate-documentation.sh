#!/bin/bash

BASEDIR=$(dirname $0)
DOCUMENTDIR=`readlink -f "$BASEDIR/../documentation"`
PARSEDIR=`readlink -f "$BASEDIR/../source/RaisePartners"`

phpdoc -d $PARSEDIR -t $DOCUMENTDIR -p
